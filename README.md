# Open Source Real Estate Buying Guide

## Why Work Without an Agent

### Cost

Real Estate agents and brokers absorb between 6% and 10% of the total cost of buying a house. An estimated 6.45 Million homes changed hands in 2021<sup>[\[1\]](https://www.statista.com/statistics/226144/us-existing-home-sales/)</sup>, with a median house price of $293,349<sup>[\[2\]](https://www.fool.com/the-ascent/research/average-house-price-state/)</sup>. That means real estate brokers account for between $17,600 and $29,350 of the average home's cost which means that on the low end, across America  $113,526,063,000 being siphoned off by an industry that provides a service which effectively babysits their clients. This is designed to be a DIY guide to buying real estate and take the fear out of a seemingly overwhelming, but otherwise simple process.

### The Fundamental Conflict of Interest for Agents

Real estate agents work on commission. This means that no matter how many houses they take you to see, no matter how much work they put into finding you a house, the only thing that impacts how much they get paid is the price of the home you buy. This means they are incentivized to get you to buy something as quickly as possible and at as high of a price as possible. This will reflect in the way they advise you to offer, so if you do choose to work with one, watch this very closely.

### Training, Or Lack There Of

To become an agent, you need
- High school Diploma 
- Take and pass a 90 hour course
- Pass the state licensing test 

I legitimately did all of this during a 10 day COVID-19 quarantine for under $400 and learned very little. If you want a little confidence boost to not work without an agent, sign up for the course. If you're in a market where you have to work with an agent, take it one step further, pass the test and become your own agent.

## Why Work With an Agent

Ok, so in effort not to appear biased, let's do this.

According to [this link I found on Google](https://sira.org/index.php/consumers/why-use-a-realtor), these are the top 7 reason to use a realtor. I'll comment in *italics* below each.

1. Act as an expert guide. Buying a home typically requires a variety of forms, reports, disclosures, and other legal and financial documents. A knowledgeable real estate agent will know what's required in your market, helping you avoid delays and costly mistakes. Also, there’s a lot of jargon involved in a real estate transaction; you want to work with a professional who can speak the language. 

*Google and your title company will do this for free. So will this guide.*

2. Offer objective information and opinions. A great real estate agent will guide you through the home search with an unbiased eye, helping you meet your buying objectives while staying within your budget. Agents are also a great source when you have questions about local amenities, utilities, zoning rules, contractors, and more. 

*L.O.L. See above on the fundamental conflict of interest for agents.*

3. Give you expanded search power. You want access to the full range of opportunities. Using a cooperative system called the multiple listing service, your agent can help you evaluate all active listings that meet your criteria, alert you to listings soon to come on the market, and provide data on recent sales. Your agent can also save you time by helping you winnow away properties that are still appearing on public sites but are no longer on the market. 
 
*There are multiple public websites that do this.*

4. Stand in your corner during negotiations. There are many factors up for discussion in any real estate transaction—from price to repairs to possession date. A real estate professional who’s representing you will look at the transaction from your perspective, helping you negotiate a purchase agreement that meets your needs and allows you to do due diligence before you’re bound to the purchase. 

*See above on the fundamental conflict of interest for agents*

5. Ensure an up-to-date experience. Most people buy only a few homes in a lifetime, usually with quite a few years between purchases. Even if you’ve bought a home before, laws and regulations change. Real estate practitioners may handle hundreds or thousands of transactions over the course of their career. 

*This is without a doubt the best reason on this list. However, real estate attorneys are actual experts in laws and regulations and are cheaper than agents*

6. Be your rock during emotional moments. A home is so much more than four walls and a roof. And for most buyers, a home is the biggest purchase they’ll ever make. Having a concerned, but objective, third party helps you stay focused on the issues most important to you when emotions threaten to sink an otherwise sound transaction. 

*Honestly, if emotional moments are a concern, bring a friend. See above on the fundamental conflict of interest for agents.*

7. Provide fair and ethical treatment. When you're interviewing agents, ask if they're a REALTOR®, a member of the National Association of REALTORS®. Every member must adhere to the REALTOR® Code of Ethics, which is based on professionalism, serving the interests of clients, and protecting the public. 

*Subtle dig at agents who didn't pay money to be in the club. Nice.*


## What If Your Market Demands You Work With an Agent?

### Discount Agents

If you live in one of [these states](https://www.caare.org/statesthatprohibitfeenegotiations/), then you can skip this section because fee negotiation is illegal where you live. Some agents will offer rebates for working with them. What this looks like is the agent will work like normal, but will pay you a percent of their fee as a rebate. Expect it to be 33-50% of the commission rate for your area. This is a cash payment to you that is not taxable and will just adjust your cost basis of the house by the amount of the rebate. If you are nervous about going into home buying without an agent or sellers are refusing to show the house to independent buyers, this is the next best thing. This [webpage](https://www.caare.org/buyer-agent-who-pay-rebate/) is the best place to my knowledge to find such an agent.

Of interest, these sorts of agents are encouraged by the [Department of Justice](https://www.justice.gov/atr/rebate-calculator), so if you want an agent, I highly encourage using this sort of agent.

### What if neither of those are options? (Dual Agents)

I've done a lot of research on this, and the conclusion I've come to is the best way to extract value from an agent is to lean into the conflict of interest and make it work for you. Approach the seller's agent. Make them your agent for that house. Then they are incentivized to get you that house because they make double the money for almost no extra work. Of course they have a conflict of interest, even more so than before and want you to offer as much as possible, so doing the work yourself and independently checking everything they touch is not optional, but the subtle, indirect information about the viability of your offer as compared to your competition is the edge that is being generated here. The biggest hurdle in this may be your local state's laws. Agents may not be allowed to act as dual agents in some states and others require written consent for this sort of arrangement from the seller. Check your local state laws.

And if the agent don't land you the house, **DO NOT** keep them on as your agent. Move on to the next seller's agent. Dual agency is a practice that is illegal in most other professions because of the conflict of interest dangers, and agents willing to engage in it are not agents you want to work with once they are no longer valuable.


## Requirements to Buy a House Without an Agent

### Time

Certainly some more time is required to buy a house without an agent doing some of the work.
- It really boils down to: How much autonomy would you have trusted an agent with the biggest purchase you will probably make? If you're going to be reading over and double checking comps, contracts, etc. honestly it only takes about a half hour to write up a purchase agreement per offer on a house. 
        - If you would trust someone who is incentivized to get you to overpay to tell you how much to offer, plan on pulling comps and writing/reviewing a contract will take about 3 hours per offer
- In terms of finding houses in the first place, working without an agent will maybe add 5 minutes a day tops
- You were going to be at the showing either way, so no extra time here
- An agent would typically hold your hand through a lot of the rest of what's in this guide, but you would still be doing it, so no extra time


### Money

Really, buying a house requires money regardless of whether one uses an agent. But since an agent won't be holding your hand through this, here we go. How much you can afford does come down to what you as the buyer wants. The traditional market tries to push people to spend as much as they are able because everyone involved in the sale, from the bank to the agents to the seller benefits from buyers spending more. YMMV, but here's a few numbers to make sure you know that should provide a reasonable bound for pricing your search
- Banks consider 30-35% of your gross monthly income to be the max monthly house payment they'll approve
- Including all other debt, banks typically allow a debt-to-income ratio (i.e. the sum of minimum monthly debt payments divided by gross monthly income) to be no greater than 40%
- A popular finance personality considers 25% of your net monthly income to be the max people should spend on a house
- Don't forget to budget about 1% of the purchase price for closing fees. This would be on top of whatever down payment you are making.

## How are you paying?

You need to acquire something that indicates that you are an eligible buyer in order to make an offer. For cash, all you need is a screenshot of an account with money as proof of funds. With a loan, you need to apply to the loan that fits your needs and get a letter that says you are preapproved for a mortgage.

### Loan Types

Loans can be broken down into different families and different term lengths
- Traditional
- First Time Home buyer
- VA
- FHA
- ARM
- Jumbo
- 10 vs. 15 vs. 30 Year Loans

#### Traditional

Requirements
- Credit score 720-740 minimum
- 10-20% down payment is normally required
- Debt-to-Income Ratio less than 40%

This is the most basic loan against which all the others will be compared.

#### First Time Homebuyer

Requirements
- Have not owned a house in the last 48 months
- Credit score 720-740 minimum
- Minimum 3.5% down payment is required
- Debt-to-Income Ratio less than 40%

Typically higher interest rates than a traditional loan in exchange for the decreased down payment


#### FHA

This is a federal program to help people buy homes. Unfortunately for the program, its run by the government, which means it can be nightmarish on the seller to work with, which might make them less willing to work with you. If possible, get approved for a conventional, first time or ARM mortgage as well and offer to switch to one of those if the government does government things.

Requirements
- Credit score 500-580 minimum
- Credit history: no more than one 30–day late payment within the past 12 months. No foreclosure or bankruptcy in the past 3 years
- Down payment: 
        - 3.5% if your credit score is 580 or higher
        - 10% if your credit score is 500-579
- Debt-to-Income less than 45%. 
        - May be higher with exceptional credit score or large cash reserves
- FHA inspection of the property. This is more detailed than a usual inspection and typically the purchase hinges on the property passing. Even for little things that would typically get ignored in a normal inspection, such as requiring a door be placed in the opening between two rooms because the inspector decided there should be a door there.


#### VA

This is a federal program to help veterans buy houses. Unfortunately for the program, its run by the government, which means it can be nightmarish on the seller to work with, which might make them less willing to work with you. If possible, get approved for a conventional, first time or ARM mortgage as well and offer to switch to one of those if the government does government things.

[Requirements](https://www.va.gov/housing-assistance/home-loans/eligibility/)
- A lot of the usual requirements exist like a conventional or FHA mortgage, but vary by lender


#### ARM

Requirements
- Vary widely

These are loans that offer lower interest rates for the first N years of the loan and higher, market driven interest rates for the rest of the loan term. These are excellent options if you plan on refinancing or selling before the end of the first N years. YMMV

#### JUMBO

Jumbo loans exist in Traditional and ARM variants. They are simply loans that exceed the cap that our overlords at Fanny Mae will buy up. As such, uniform standards don't really exist and vary based on the risk appetite of the bank issuing the loan.

#### 10 vs. 15 vs. 30 Year Loans

YMMV. Choosing a loan term depends on personal choice and opportunity cost. If you can afford a 15 year mortgage, but got a 30 year mortgage instead, what would you do with the extra money that you aren't paying into the house? Is it earning money? Is that money greater than the cost of the extra interest accruing?

# TODO example


## Searching for a house

Traditionally, this has been the value that realtors brought to the table. The internet has changed that. These are three good options.

- [Zillow](https://www.zillow.com/)
- [Realtor.com](https://www.realtor.com/)
- [Trulia](https://www.trulia.com/)

There was a lot of Zillow hate going around Summer/Fall 2021, and I just want to be clear. Most of that was misinformation in what appeared to be a, coordinated or not, smear campaign by bad actors who presumably believed they had something to gain from spreading the misinformation. Possibly not unrelated, real estate agents I have talked to view Zillow as a threat to their business model. Zillow is a notable improvement over the old school real estate process where only agents have access to listings and is not somewhere to avoid.


## Touring a house

Now that you have a house you want to see, contact the seller or the seller's agent based on the contact info in the listing. Set up a time to go look at it. If they don't play ball and want you to have an agent, which is not uncommon in competitive markets, look into the section above on rebate agents and/or dual agents.

When you are at the home, there are a number of things you want to look for. The trick is to not get bogged down in the novelty of the home and see through to what it would actually be like to live with. Also, take pictures. Of every room. Of every issue you find. Not sure what something is? Take a picture of it and look it up later.

My suggestion is make two passes through every room, the first in inspector mode, looking for instability/cracks in the foundation, rotting roofing/soffit/fascia, signs of water in places where there shouldn't, or lack of water in places where there should be water. These are typically the most expensive of issues. The second pass is the one where you think "do I like this room and what would I do with it?"

As soon as you finish looking at the house, decide if you plan on pursuing it further. 

- If you are, ask the seller for the disclosures list and take a look at that.
- If you aren't, its customary in some markets to write a brief note to the seller why, though not strictly necessary.

## Finding Comparables

1. Go on Zillow
2. Click on "For Sale" and change it to "Sold"
3. Go to more filters, find "Sold In Last" and select "90 Days"
4. Zoom in relatively close to the house/condo you're looking (here in called the principle) at to where you have at least 3-4 comparable sales

        If there aren't enough comparables 1) in the same neighborhood, 2) that cover all differences, expand your search incrementally to maybe a 1 mile radius and/or 4 months back
        If you still need more, alternate between inching the time window further back (by a month at a time or so) and the radius by as little as possible
        A comparable that requires the fewest adjustments is the best comparable and time and location are the 2 hardest to simply adjust for, but if limited on comparables, adjusting for these may become more ideal than adjusting for wildly different examples
        It's ok to get comparables for your comparables to help estimate adjustments, but don't include them in the key metrics

5. Open a spreadsheet and list out key differences. Common key differences include

        Square footage
        Number of bedrooms
        Number of bathrooms and full bathrooms
        Existence of or absence of a basement
        Number of car garage
        Appliance age
        HVAC age (Disclosures list will come in handy here)
        Roof age/condition
        Cash sale vs. Mortgage
6. Value each difference and adjust the value of the comparable **NEVER ADUST THE VALUE OF THE PRINCIPLE**
7. Sum all the adjustments up and create an estimated comparable value
8. Calculate key metrics

        Average the comparable values
        Take the median comparable value
        Calculate price trend over time
        If an outlier exists, take an average without it
        If you have enough for a rough standard deviation calculation, that's great

9. How do these metrics line up with the asking price? If you have the same number, congrats, you replicated the seller's process. Even better, it means you are dealing with a reasonable seller. Keep this spreadsheet in case of counter offers. Having data driven metrics to indicate why your offer is reasonable is useful negotiation tool.

# TODO example

## Select a Title Company

The title company is the entity that will facilitate the transaction. When you give them a signed purchase agreement, they are the ones who

1. Do a full history of the ownership of the property and verify that the seller does in fact own and have the right to sell the property
2. Facilitate receiving the money from the buyer and distributing it to the seller
3. Executing and recording the deed on the buyer's behalf

Additionally they may have additional roles depending on the existence of and/or competence of realtors involved in the transaction
4. They can be the ones who handle receipt of the earnest money
5. They can provide blank purchase agreement documents


If you have no idea where to start, [Consumer Advocates in American Real Estate](https://www.caare.org/independenttitlecompanies/) keeps a list of independent (unaffiliated with any real estate brokerages) title companies.

Selecting a title company will have different criteria based on your situation. If you're an independent buyer, picking one that works with independent buyers is first priority. A good to have is one that is completely unaffiliated with any real estate agency involved in the transaction. Using an independent title company is one way to go about that. Then things like their BBB rating. Call them, ask some question you may have or ask for a blank purchase agreement, are they helpful? The title company is the most important entity in facilitating a transaction, so don't take this step too lightly.

As a buyer, you have 100% control of your title company selection, unless you cede it to the seller. There should be a line in the purchase agreement for it that states
"Pursuant to Federal and State Law, Seller cannot make Seller's selection of a title insurance provider a condition of this agreement."
It is your right as the buyer, use it.


## Deciding how much to offer

1. How much do you want this?
        How replaceable is it? 
        Will an identical one down the street be for sale in a week?
        If you really want it and it appears unlikely that an identical one will be for sale soon, factor that into your offer
2. The market 
        Is everything on the market selling in 1-3 days and this has been on the market for 2 weeks? 
        Did your recent comparables sell for at, above or below asking price? 
3. If you don't have an agent and the seller is willing to work with a buyer's agents
        Look up what the going rate of Real estate agent commission in your area is (Usually between 6-10%)
        Divide that commission by 2 to get the buyer's agent commission
        Subtract off that percent of what you would have offered from your offer
        Highlight this fact in your offer email so it's clear that they pay no buyer's agent commission

        Some sellers will specify they only pay X% buyer's agent commission
        In this case, just remove X% from your offer amount instead


These factors will influence your decision on how much to offer. If the going rate in the market is X above asking, then you should probably offer X above asking if you really want it. If it's been sitting on the market longer than average, understand why. What is uniquely impacting its desirability? Factor that in.

Alternatively, if you trust your comparable math and are willing to maybe lose to a better offer, just offer whatever your comparables indicated (less buyer's agent commission if not applicable).



# TODO graph

## Making an offer

Real Estate agents may say this is where their value is, but they are not contract or legal experts. First, look up if you live in one of the 22 states that require a Real Estate Attorney. If so, you're paying one anyway, you should be consulting them here. Even if not, attorneys are subject matter experts **AND CHEAPER** than real estate agents, so if something about the transaction has complicated legal aspects, check with an attorney if you're unclear of the implications. (EX: easements, shared utilities, etc.)

1. Find a blank copy of your state's Improved Property Purchase Agreement. This is most readily acquired from your title company.
2. Fill it out
3. If applicable, forward to your attorney
4. Make necessary changes
5. If applicable, forward to your real estate agent
6. Either you send this and your preapproval letter or proof of funds to the seller or the real estate agent will at which point, you have made an offer

# TODO example

## Accepted offer

Congratulations, now for some busywork. You have to contact 4 places 
1. Inspection Company
2. Earnest Money
3. Mortgage Lender
4. Title Company
5. Insurance Company

## Inspection

Find an inspection company. Schedule an inspection with them. Let the seller know when the inspection is scheduled for. Also, word of advice, this is not a place to cut corners on cost. Get a good one.

Once you get the report back from the inspector, you have to respond to it either that you accept the condition or that you would like X done to mitigate Y or a credit of Z for Y.

## Earnest Money

Earnest money needs to be dropped off within the time specified in the sales contract. Usually 2 days. It usually gets dropped off at either the title company or the seller's broker's office. If the seller's broker's office is unreasonably far away, push to use the title company or if you have your own agent, and your state allows it, dump the responsibility of earnest money delivery on your agent.

## Title Company

The title company will need
1. A copy of the purchase agreement
2. Name, Email, and phone number of the buyer, seller, buyer and seller agents (if applicable), lender and loan officer/primary contact
3. How you would like to take Title. You will have to check what the options are in your state

## Insurance

Get a quote for home insurance from your favorite insurance provider and connect your insurance agent with your loan officer from your lender. Note, if you need flood insurance, your mortgage lender may offer it. 999/1000 times it is cheaper and better to go with an actual insurance company. 

## Mortgage Lender

The lender will need a few things from you.
1. Signed purchase agreement
2. Signed seller's disclosure
3. Listing
4. Insurance quote
5. Copy of the earnest money check

The mortgage lender will arrange the appraisal. They will make you pay for it though. Once you pay for it, the only thing a buyer needs to worry about here is if the property appraises for less than the purchase price. If it does that, either you need to go back to the seller and renegotiate or cover the gap between appraisal and purchase price in cash or some mix of the two.

Once all this is complete, all the information will go to the mortgage underwriter who will write up the mortgage. Once this is complete, you are ready to close

## Closing

1. Remember to wire the cash required to close money to the title company the day **before** closing.
2. Make sure to touch base with the seller a few days **before** closing and find out which utilities need to be switched and contact those companies to switch them.
3. The title company will hold your hand through the closing process. Be prepared to sign a lot of things. Odds are, everything will be in order, but keep your eyes peeled and don't be afraid to ask questions.


## FAQ

- As Is offers refer to accepting all contents of the seller's disclosure form and nothing more. Anything found in inspection beyond that, no matter how minor is not covered, unless otherwise specified.
- If you've submitted an offer and decide you no longer want to, you can freely withdraw your offer at any point before the seller responds.


## So You Have To Back Out Of A Purchase Agreement


### I haven't paid my earnest money yet

If you haven't delivered the earnest money, in most states (check your local laws), the deal is not considered binding until the earnest money is deposited, so backing out should be very straightforward without loss of money. Be very careful if someone other than an attorney tells you differently.

# I have paid the earnest money

Get a blank copy of your state's "Mutual Release From Purchase Agreement."
Read the contract. Do you have grounds to back out? 

#### No

Be prepared to lose your earnest money deposit. Fill out the "Mutual Release From Purchase Agreement" appropriately, and send it via email to the seller.

#### Yes
 
Fill out the "Mutual Release From Purchase Agreement" appropriately, and send it via email to the seller saying something like "As a result of X, please cancel this purchase agreement. Per line Y of the purchase agreement, I am entitled to a full earnest money refund."

You will get pushback, particularly from agents. I've seen manipulative/strong arm/bully tactics, as well as illegal tactics such as practicing law without a license. If the contract gives you an out and you have the proof to back it up, you will get that money back. Offer to bring a real estate attorney to the table. And bring one to the table if that's what it takes. If you collect all the right information, 15-30 minutes for a lawyer to draft a letter and send it out should sort things right out.

The other thing that is worth knowing here is the leverage differential that exists here. Once an offer is accepted, the seller faces steep penalties for entertaining other offers while under contract. The buyer on the other hand may continue to offer on other houses. Thus, as soon as you make it clear that you are not moving forward with the purchase, and have a good enough legal basis to tie up the house in small claims for a while, fighting it is rarely in the cards for the seller since they have little to gain and much to lose in missing time on the market.

Lastly once you are out, check with your insurance, because while the mortgage lender orders insurance from the policy quote you provide, it is your responsibility to cancel that insurance.
